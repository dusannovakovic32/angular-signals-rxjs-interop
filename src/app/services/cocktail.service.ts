import { Injectable, inject } from '@angular/core';
import { Cocktail } from '../shared/cocktail';
import { BehaviorSubject, Observable, map, take } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment'
import { generateCocktail } from '../shared/helpers';

@Injectable({
  providedIn: 'root'
})
export class CocktailService {
  private readonly http = inject(HttpClient);

  private readonly RANDOM_COCKTAILS_URL = environment.api.baseURL + "randomselection.php";
  private readonly SEARCH_BY_NAME_URL = environment.api.baseURL + "search.php";

  private readonly randomCocktails = new BehaviorSubject<Cocktail[]>([]);
  randomCocktails$ = this.randomCocktails.asObservable();

  constructor() {
    this.emitRandomCocktails();
  }
  
  emitRandomCocktails(): void {
    this.http.get<Cocktail[]>(this.RANDOM_COCKTAILS_URL)
      .pipe(
        map((response: any) => {
          return (response.body ?? response).drinks.map((element: any) => {
            return generateCocktail(element);
          })
        }),
        take(1)
      ).subscribe(cocktails => this.randomCocktails.next(cocktails));
  }

  searchCocktailsByName(searchTerm: string): Observable<Cocktail[]> {
    const options = { params: new HttpParams().set('s', searchTerm) };

    return this.http.get<Cocktail[]>(this.SEARCH_BY_NAME_URL, options).pipe(
      map((response: any) => {
        if (!response.drinks)
          return [];

        return response.drinks.map((element: any) => generateCocktail(element));
      }),
    );
  }
}