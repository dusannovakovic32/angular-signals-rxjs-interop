import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { NgFor } from '@angular/common';
import { MatCardModule } from '@angular/material/card'
import { Cocktail } from 'src/app/shared/cocktail';

@Component({
  selector: 'app-cocktail-card[cocktail]',
  standalone: true,
  imports: [NgFor, MatCardModule],
  templateUrl: './cocktail-card.component.html',
  styleUrls: ['./cocktail-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CocktailCardComponent {
  @Input() cocktail!: Cocktail;
}