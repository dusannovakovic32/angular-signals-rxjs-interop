import { ChangeDetectionStrategy, Component, Signal, computed, inject } from '@angular/core';
import { NgFor } from '@angular/common';
import { Cocktail } from 'src/app/shared/cocktail';
import { debounceTime, distinctUntilChanged, of, switchMap } from 'rxjs';
import { CocktailCardComponent } from '../cocktail-card/cocktail-card.component';
import { FormControl, ReactiveFormsModule } from '@angular/forms';
import { toSignal } from '@angular/core/rxjs-interop';
import { CocktailService } from 'src/app/services/cocktail.service';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatTooltipModule } from '@angular/material/tooltip';

@Component({
  selector: 'app-home',
  standalone: true,
  imports: [NgFor, CocktailCardComponent, ReactiveFormsModule, MatButtonModule, MatIconModule, MatTooltipModule],
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomeComponent {
  private readonly _cocktailService = inject(CocktailService);
  readonly SEARCH_TERM_MIN_LENGTH = 3;

  private searchedCocktails: Signal<Cocktail[]>;
  private randomCocktails!: Signal<Cocktail[]>;
  cocktailsList: Signal<Cocktail[]>; 

  searchTerm: FormControl<string>;

  constructor() {
    this.searchTerm = new FormControl('', {nonNullable: true});

    this.randomCocktails = toSignal(this._cocktailService.randomCocktails$, {initialValue: []});
    
    this.searchedCocktails = toSignal(
      this.searchTerm.valueChanges
        .pipe(
          debounceTime(300),
          distinctUntilChanged(),
          switchMap(searchTerm => {
            if (searchTerm.length < this.SEARCH_TERM_MIN_LENGTH) {
              return of([]);
            }

            return this._cocktailService.searchCocktailsByName(searchTerm);
          })
        ),
        { initialValue: [] }
        );
        
    this.cocktailsList = computed(() => {
      if (this.searchedCocktails().length) {
        return [...this.searchedCocktails()]
      }
  
      return [ ...this.randomCocktails()]
    });
  }
  
  onRefresh() {
    this._cocktailService.emitRandomCocktails();
    this.searchTerm.setValue('');
  }

  trackByFn(index: number, item: Cocktail) {
    return item.id;
  }
}