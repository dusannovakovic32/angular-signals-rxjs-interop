import { Cocktail } from "./cocktail";

export function generateCocktail(apiDto: any): Cocktail {
  return {
    id: apiDto.idDrink,
    name: apiDto.strDrink,
    alcoholic: apiDto.strAlcoholic,
    category: apiDto.strCategory,
    image: apiDto.strDrinkThumb,
    glass: apiDto.strGlass,
    instructions: apiDto.strInstructions,
    ingredients: generateIngredientsLikeAnEightGrader(apiDto)
  };

}

// API is a bit weird
function generateIngredientsLikeAnEightGrader(apiDto: any): any [] {
  return [
    {
      name: apiDto.strIngredient1,
      measure: apiDto.strMeasure1
    },
    {
      name: apiDto.strIngredient2,
      measure: apiDto.strMeasure2
    },
    {
      name: apiDto.strIngredient3,
      measure: apiDto.strMeasure3
    },
    {
      name: apiDto.strIngredient4,
      measure: apiDto.strMeasure4
    },
    {
      name: apiDto.strIngredient5,
      measure: apiDto.strMeasure5
    },
    {
      name: apiDto.strIngredient6,
      measure: apiDto.strMeasure6
    },
    {
      name: apiDto.strIngredient7,
      measure: apiDto.strMeasure7
    },
    {
      name: apiDto.strIngredient8,
      measure: apiDto.strMeasure8
    },
    {
      name: apiDto.strIngredient9,
      measure: apiDto.strMeasure9
    },
    {
      name: apiDto.strIngredient10,
      measure: apiDto.strMeasure10
    }
  ].filter(ingredient => ingredient.measure && ingredient.name)
}